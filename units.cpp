#include <assert.h>
#include <math.h>
#include <stdlib.h>

#include <iostream>
#include <vector>

#include "option.h"
#include "option.hh"

enum KindError { OverConstrained, UnderConstrained, Positivity };

struct UnitsException : public std::exception {
    UnitsException(const KindError &kind_, const std::string &id_)
        : std::exception(), kind(kind_), id(id_) {
        switch (kind) {
            case OverConstrained:
                id += " : is over constrained";
                break;
            case UnderConstrained:
                id += " : is under constrained";
                break;
            case Positivity:
                id += " : must be positive";
                break;
            default:
                id = "The exception is ill defined.";
                break;
        }
    }

    // this produces a tidy warning. But it's not compiling without c++20
    // otherwise.
    [[nodiscard]] const char *what() const noexcept final {
        return id.c_str();
    }

private:
    KindError kind;
    std::string id;
};

template <typename T>
void positivity(T val, const std::string &s) {
    if (val <= T()) {
        throw UnitsException(Positivity, s);
    }
}

template <typename T>
class Units {
public:
    class Builder;

    auto print() const {
        std::cout << "Re = " << Re << std::endl;
        std::cout << "charLengthLb = " << charLengthLb << ", charLengthPh = " << charLengthPh
                  << std::endl;
        std::cout << "uLb = " << uLb << ", uPh = " << uPh << std::endl;
        std::cout << "nuLb = " << nuLb << ", nuPh = " << nuPh << std::endl;
        std::cout << "deltaX = " << deltaX << ", deltaT = " << deltaT << std::endl;
        std::cout << "omega = " << omega << std::endl;
        std::cout << "nx = " << nx << ", ny = " << ny << ", nz = " << nz << std::endl;
    }

    [[nodiscard]] auto getNx() const -> int {
        return nx;
    }

    [[nodiscard]] auto getNy() const -> int {
        return nx;
    }

    [[nodiscard]] auto getNz() const -> int {
        return nx;
    }

    [[nodiscard]] auto getOmega() const -> T {
        return omega;
    }

private:
    Units(T Re_, T charLengthLb_, T charLengthPh_, T uLb_, T uPh_, T nuLb_, T nuPh_, T deltaX_,
          T deltaT_, T omega_, T xLengthPh_, T yLengthPh_, T zLengthPh_)
        : Re(Re_),
          charLengthLb(charLengthLb_),
          charLengthPh(charLengthPh_),
          uLb(uLb_),
          uPh(uPh_),
          nuLb(nuLb_),
          nuPh(nuPh_),
          deltaX(deltaX_),
          deltaT(deltaT_),
          omega(omega_),
          xLengthPh(xLengthPh_),
          yLengthPh(yLengthPh_),
          zLengthPh(zLengthPh_) {
        nx = static_cast<int>(xLengthPh / deltaX + 0.5) + 1;
        ny = static_cast<int>(yLengthPh / deltaX + 0.5) + 1;
        nz = static_cast<int>(zLengthPh / deltaX + 0.5) + 1;
    }

private:
    T Re;
    T charLengthLb, charLengthPh;
    T uLb, uPh, nuLb, nuPh;
    T deltaX, deltaT, omega;

    T xLengthPh, yLengthPh, zLengthPh;
    int nx, ny, nz;
};

template <typename T, typename U>
auto have_values(opt::Option<T> &lhs, opt::Option<U> &rhs) -> bool {
    return lhs.has_value() && rhs.has_value();
}

template <typename T, typename U, typename V>
auto have_values(opt::Option<T> &lhs, opt::Option<U> &chs, opt::Option<V> &rhs) -> bool {
    return lhs.has_value() && chs.has_value() && rhs.has_value();
}

// deltaX = charLengthPh / charLengthLb

template <typename T>
class Units<T>::Builder {
public:
    // set functions automaticalyLengthPh assign values to
    // members when it is possible.
    auto setRe(T Re_) -> Builder & {
        if (have_values(charLengthPh, uPh, nuPh) || have_values(charLengthLb, uLb, nuLb)) {
            throw UnitsException(OverConstrained, "Re");
        };
        Re = Re.unique_set(Re_);
        tryComputeAll();

        return *this;
    }

    auto setNuLb(T nuLb_) -> Builder & {
        if (have_values(deltaT, deltaX, nuPh) || have_values(charLengthLb, uLb, Re) ||
            omega.has_value()) {
            throw UnitsException(OverConstrained, "nuLb");
        };
        nuLb = nuLb.unique_set(nuLb_);
        tryComputeAll();

        return *this;
    }

    auto setNuPh(T nuPh_) -> Builder & {
        if (have_values(deltaT, deltaX, nuLb) || have_values(charLengthPh, uPh, Re)) {
            throw UnitsException(OverConstrained, "nuPh");
        };
        nuPh = nuPh.unique_set(nuPh_);
        tryComputeAll();

        return *this;
    }

    auto setCharLengthLb(T charLengthLb_) -> Builder & {
        if (have_values(charLengthPh, deltaX) || have_values(Re, uLb, nuLb)) {
            throw UnitsException(OverConstrained, "charLengthLb");
        };
        charLengthLb = charLengthLb.unique_set(charLengthLb_);
        tryComputeAll();

        return *this;
    }

    auto setCharLengthPh(T charLengthPh_) -> Builder & {
        if (have_values(charLengthLb, deltaX) || have_values(Re, uPh, nuPh)) {
            throw UnitsException(OverConstrained, "charLengthPh");
        };
        charLengthPh = charLengthPh.unique_set(charLengthPh_);
        tryComputeAll();

        return *this;
    }

    auto setDeltaX(T deltaX_) -> Builder & {
        if (have_values(charLengthPh, charLengthLb)) {
            throw UnitsException(OverConstrained, "deltaX");
        }
        if (have_values(uPh, uLb, deltaT)) {
            throw UnitsException(OverConstrained, "deltaX");
        }
        deltaX = deltaX.unique_set(deltaX_);
        tryComputeAll();

        return *this;
    }

    // uLb = uPh * deltaT / deltaX

    auto setUlb(T uLb_) -> Builder & {
        if (have_values(uPh, deltaX, deltaT)) {
            throw UnitsException(OverConstrained, "uLb");
        }
        uLb = uLb.unique_set(uLb_);
        tryComputeAll();

        return *this;
    }

    auto setUph(T uPh_) -> Builder & {
        if (have_values(uLb, deltaX, deltaT)) {
            throw UnitsException(OverConstrained, "uPh");
        }
        uPh = uPh.unique_set(uPh_);
        tryComputeAll();

        return *this;
    }

    auto setDeltaT(T deltaT_) -> Builder & {
        if (have_values(uPh, deltaX, uLb) || have_values(nuPh, deltaX, nuLb)) {
            throw UnitsException(OverConstrained, "deltaT");
        }
        deltaT = deltaT.unique_set(deltaT_);
        tryComputeAll();

        return *this;
    }

    auto setOmega(T omega_) -> Builder & {
        if (nuLb.has_value()) {
            throw UnitsException(OverConstrained, "deltaT");
        }
        omega = omega.unique_set(omega_);
        tryComputeAll();

        return *this;
    }

    auto setLxLyLz(T xLengthPh_, T yLengthPh_, T zLengthPh_) -> Builder & {
        if (!(xLengthPh_ >= T())) {
            throw UnitsException(Positivity, "xLengthPh");
        }
        if (!(yLengthPh_ >= T())) {
            throw UnitsException(Positivity, "yLengthPh");
        }
        if (!(yLengthPh_ >= T())) {
            throw UnitsException(Positivity, "zLengthPh");
        }

        xLengthPh = xLengthPh.unique_set(xLengthPh_);
        yLengthPh = yLengthPh.unique_set(yLengthPh_);
        zLengthPh = zLengthPh.unique_set(zLengthPh_);

        return *this;
    }

    // Some values must fullfill some contrains (omega <= 2, uLb << 1, ...).
    // All quantities must be positive
    auto checkConsistency() -> bool {
        if (omega.value() > T(2) || omega.value() <= T(0)) {
            return false;
        }
        // The max value of uLb should rater be 0.5 but eh sometimes
        // we may want to go to the limit.
        if (uLb.value() >= T(1) || uLb) {
            return false;
        }

        return true;
    }

    auto build() -> Units<T> {
        tryComputeAll();
        Units<T> units(Re.try_value("Re"), charLengthLb.try_value("charLengthLb"),
                       charLengthPh.try_value("charLengthPh"), uLb.try_value("uLb"),
                       uPh.try_value("uPh"), nuLb.try_value("nuLb"), nuPh.try_value("nuPh"),
                       deltaX.try_value("deltaX"), deltaT.try_value("deltaT"),
                       omega.try_value("omega"), xLengthPh.try_value("xLengthPh"),
                       yLengthPh.try_value("yLengthPh"), zLengthPh.try_value("zLengthPh"));
        return units;
    }

    [[nodiscard]] auto print(std::string pre) const {
        std::cout << "======== " << pre << "=======" << std::endl;
        std::cout << "Re = " << Re << std::endl;
        std::cout << "charLengthLb = " << charLengthLb << ", charLengthPh = " << charLengthPh
                  << std::endl;
        std::cout << "uLb = " << uLb << ", uPh = " << uPh << std::endl;
        std::cout << "nuLb = " << nuLb << ", nuPh = " << nuPh << std::endl;
        std::cout << "deltaX = " << deltaX << ", deltaT = " << deltaT << std::endl;
        std::cout << "omega = " << omega << std::endl;
    }

private:
    auto tryComputeAll() {
        // dx = lPh / lLb
        charLengthPh = charLengthPh.maybe_set(charLengthLb * deltaX);
        // Re = u * l / nu
        charLengthPh = charLengthPh.maybe_set(Re * nuPh / uPh);
        // dx = lPh / lLb
        charLengthLb = charLengthLb.maybe_set(charLengthPh / deltaX);
        // Re = u * l / nu
        charLengthLb = charLengthLb.maybe_set(Re * nuLb / uLb);

        // dx = lPh / lLb
        deltaX = deltaX.maybe_set(charLengthPh / charLengthLb);
        // uPh = uLb * dx / dt
        deltaX = deltaX.maybe_set(uPh * deltaT / uLb);
        // nuP = nuLb * dx^2/dt
        deltaX = deltaX.maybe_set((nuPh * deltaT / nuLb).map([](T a) {
            return sqrt(a);
        }));

        // uPh = uLb * dx / dt
        deltaT = deltaT.maybe_set(uLb * deltaX / uPh);
        // nuP = nuLb * dx^2/dt
        deltaT = deltaT.maybe_set(nuLb * deltaX * deltaX * nuPh);

        // uPh = uLb * dx / dt
        uPh = uPh.maybe_set(uLb * deltaX / deltaT);
        uLb = uLb.maybe_set(uPh * deltaT / deltaX);

        // nuP = nuLb * dx^2/dt
        nuPh = nuPh.maybe_set(nuLb * deltaT * deltaX / deltaX);
        nuLb = nuLb.maybe_set(nuPh * deltaX * deltaX / deltaT);

        // Re = u * l / nu
        nuPh = nuPh.maybe_set(uPh * charLengthPh / Re);
        nuLb = nuLb.maybe_set(uLb * charLengthLb / Re);

        // TODO(malaspor): we need a descriptor here
        // nuLb = cs2 * (1/omega - 1/2) = cs2 * (2 - omega) / (2*omega)
        nuLb = nuLb.maybe_set(opt::Option<T>(1.0 / 3.0) / omega - opt::Option<T>(0.5));

        // TODO(malaspor): we need a descriptor here
        // omega = 1/(nuLb / cs2 + 1/2) = 2 * cs2 / (2 * nuLb + cs2)
        omega = omega.maybe_set(opt::Option<T>(2.0 / 3.0) /
                                (opt::Option<T>(2.0) * nuLb + opt::Option<T>(1.0 / 3.0)));

        // Re = u * l / nu
        Re = Re.maybe_set(uLb * charLengthLb / nuLb);
        Re = Re.maybe_set(uPh * charLengthPh / nuPh);
    }

private:
    opt::Option<T> Re;
    opt::Option<T> charLengthLb, charLengthPh, uLb, uPh, nuLb, nuPh;
    opt::Option<T> deltaX, deltaT, omega;

    opt::Option<T> xLengthPh, yLengthPh, zLengthPh;
};

auto main() -> int {
    Units<double> units = Units<double>::Builder{}
                              .setUlb(0.01)
                              .setUph(1.0)
                              .setCharLengthPh(2.0)
                              .setCharLengthLb(10)
                              //   .setDeltaX(0.2)
                              .setLxLyLz(2.0, 4.5, 3.9)
                              .setRe(10.0)
                              .build();

    units.print();

    return EXIT_SUCCESS;
}