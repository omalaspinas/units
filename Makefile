CC=g++
OPTS=-Wall -Wextra -std=c++17 -g -pedantic -fsanitize=address -fsanitize=undefined
TIDY=clang-tidy
TIDY_OPTS=--format-style=file
FORMAT=clang-format
FORMAT_OPTS=-i

units: units.o
	$(CC) $(OPTS) -o units units.o

units.o: units.cpp option.h option.hh
	$(CC) $(OPTS) -c units.cpp

clang_tidy: units.cpp
	$(TIDY) $(TIDY_OPTS) units.cpp -- $(OPTS)

clang_tidy_fix: units.cpp
	$(TIDY) $(TIDY_OPTS) -fix units.cpp -- $(OPTS)

clang_format: units.cpp
	$(FORMAT) $(FORMAT_OPTS) units.cpp

clean:
	rm -f *.o units
	make clean -C tests
